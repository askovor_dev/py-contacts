import os
import sys
import logging
from dotenv import load_dotenv
from flask import (Flask, redirect)
from flask_restful import Api
from flask_swagger_ui import get_swaggerui_blueprint
from database import database as db
from resources import SWAGGER_URL, API_SPEC_URL
from resources.contact import Contact, ContactList
from resources.contact_email import ContactEmail

def create_app():
    try:
        app = Flask(__name__)
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['PROPAGATE_EXCEPTIONS'] = os.getenv('PROPAGATE_EXCEPTIONS', True)
        app.secret_key = os.getenv('SECRET_KEY', 'placeholder')
        return app
    except:
        _logger.error("Internal error: %s", sys.exc_info())
        raise

def create_api(app):
    try:
        api = Api(app)
        api.add_resource(ContactList, '/contact')
        api.add_resource(Contact, '/contact/<string:username>')
        api.add_resource(ContactEmail, '/contact/<string:username>/email/<string:address>')
        return api
    except:
        _logger.error("Internal error: %s", sys.exc_info())
        raise

_logger = logging.getLogger(__name__)
swaggerui_blueprint = get_swaggerui_blueprint(SWAGGER_URL, API_SPEC_URL)
load_dotenv(verbose=True)

app = create_app() 
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI','sqlite:///contacts.db')
api = create_api(app)

@app.route("/")
def home():
    return redirect(SWAGGER_URL)

@app.before_first_request
def create_tables():
    db.create_all()

if __name__ == '__main__':
    db.init_app(app)
    app.run(port=5000)