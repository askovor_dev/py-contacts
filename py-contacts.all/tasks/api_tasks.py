import os
import sys
import logging
from celery import Celery
from celery.schedules import crontab
from apiclient.client import Client
from datetime import (datetime, timedelta)
import dateutil.parser as dt_parser
from dotenv import load_dotenv
from helpers.random_generators import (random_str_generator, random_name_generator)
from apiclient import LOCALHOST 

_logger = logging.getLogger(__name__)

load_dotenv(verbose=True)
api_host_url = os.getenv('API_HOST_URL', LOCALHOST)
_logger.info("API_HOST_URL set to '%s'", api_host_url)

# celery -A tasks.api_tasks worker -B --loglevel=info
try:
    app = Celery('api_tasks')
    app.config_from_object('tasks.celeryconfig')
    app.conf.beat_schedule = {
        'create-contact-every-15-sec': {
            'task': 'tasks.api_tasks.create_random_contact',
            'schedule': 15.0,
            'args': {10},
        },
        'clean-contacts-every-60-sec': {
            'task': 'tasks.api_tasks.clean_contacts',
            'schedule': 60.0,
            'args': {60},
        } 
    }
except:
    _logger.error("Internal error: %s", sys.exc_info())
    raise

@app.task
def create_random_contact(name_len):
    cli = Client(api_host_url)
    return cli.create_contact(
        random_name_generator(name_len),
        random_name_generator(name_len),
        random_name_generator(name_len)
    )

@app.task
def clean_contacts(exp_window):
    cli = Client(api_host_url)
    contact_list = cli.get_contact_list()
    exp_dt = datetime.utcnow() - timedelta(seconds=exp_window)
    exp_contacts = list(
        map(lambda c: c['username'],
        filter(lambda c: dt_parser.parse(c['created']) < exp_dt, contact_list))
        )
    for c in exp_contacts:
        cli.delete_contact(c)