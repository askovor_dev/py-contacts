from apiclient import LOCALHOST
from apiclient.http_requests import get, post, put, delete

class ApiError(Exception):
    pass

class Client:
    def __init__(self, site_url=LOCALHOST):
        if site_url.endswith("/"):
            self.site_url = site_url
        else:
            self.site_url = "{}/".format(site_url)

    def get_contact_list(self):
        url = "{}contact".format(self.site_url)
        res = get(url)
        if res['status_code'] == 200:
            return res['content']['contacts']
        raise ApiError(res['content']['message'] if res['status_code'] < 500 else None)

    def get_contact(self, username):
        url = "{}contact/{}".format(self.site_url, username)
        res = get(url)
        if res['status_code'] == 200:
            return res['content']
        raise ApiError(res['content']['message'] if res['status_code'] < 500 else None)

    def create_contact(self, username, firstname, lastname):
        url = "{}contact/{}".format(self.site_url, username)
        res = post(url, data = {"firstname":firstname, "lastname":lastname})
        if res['status_code'] == 201:
            return res['content']
        raise ApiError(res['content']['message'] if res['status_code'] < 500 else None)

    def update_contact(self, username, firstname, lastname):
        url = "{}contact/{}".format(self.site_url, username)
        res = put(url, data = {"firstname":firstname, "lastname":lastname})
        if res['status_code'] == 200:
            return res['content']
        raise ApiError(res['content']['message'] if res['status_code'] < 500 else None)

    def delete_contact(self, username):
        url = "{}contact/{}".format(self.site_url, username)
        res = delete(url)
        if res['status_code'] != 200:
            raise ApiError(res['content']['message'] if res['status_code'] < 500 else None)