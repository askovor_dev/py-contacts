import os
import requests

headers = {'Accept': 'application/json'}

def get(url, params=None):
    r = requests.get(url, params=params, headers=headers)
    if r.status_code < 500:
        return {'status_code': r.status_code, 'content': r.json()}
    return {'status_code': r.status_code}

def post(url, params=None, data=None):
    r = requests.post(url, params=params, headers=headers, data=data)
    if r.status_code < 500:
        return {'status_code': r.status_code, 'content': r.json()}
    return {'status_code': r.status_code}    

def put(url, params=None, data=None):
    r = requests.put(url, params=params, headers=headers, data=data)
    if r.status_code < 500:
        return {'status_code': r.status_code, 'content': r.json()}
    return {'status_code': r.status_code}    

def delete(url, params = None):
    r = requests.delete(url, params=params, headers=headers)
    if r.status_code < 500:
        return {'status_code': r.status_code, 'content': r.json()}
    return {'status_code': r.status_code} 
    
