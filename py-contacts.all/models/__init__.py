import re

_email_regex = '^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,3})$'
EMAIL_REGEX = re.compile(_email_regex)
