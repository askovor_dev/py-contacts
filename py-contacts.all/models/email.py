from database import database as db
from datetime import datetime
from sqlalchemy.sql import func

class EmailModel(db.Model):
    __tablename__ = 'emails'

    address = db.Column(db.String(40), primary_key = True)
    username = db.Column(db.String(40), db.ForeignKey('users.username'))
    created = db.Column(db.DateTime, nullable=False)
    lastupdated = db.Column(db.DateTime, nullable=False)
    user = db.relationship('UserModel', back_populates="emails", uselist=False)
    
    def __init__(self, address, username):
        self.address = address
        self.username = username
        self.created = self.lastupdated = datetime.utcnow()

    def json(self):
        return {
            'address': self.address,
            'username': self.username,
            'created': self.created.isoformat(),
            'lastupdated': self.lastupdated.isoformat()
        }

    def save(self, update=False):
        if update:
            self.lastupdated = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).order_by(cls.address).all()

    @classmethod
    def find_by_address(cls, address):
        return cls.query.filter_by(address=address).one_or_none()

    @classmethod
    def find_all(cls):
        return cls.query.order_by(cls.username).all()