import re
from models import EMAIL_REGEX

def validate_email(address):  
    return EMAIL_REGEX.fullmatch(address)