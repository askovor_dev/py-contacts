from database import database as db
from datetime import datetime
from sqlalchemy.sql import func
from models.email import EmailModel

class UserModel(db.Model):
    __tablename__ = 'users'
    username = db.Column(db.String(40), primary_key=True)
    firstname = db.Column(db.String(40), nullable=False)
    lastname = db.Column(db.String(40), nullable=False)
    created = db.Column(db.DateTime, nullable=False)
    lastupdated = db.Column(db.DateTime, nullable=False)
    emails = db.relationship(
            'EmailModel',
            back_populates='user',
            order_by='EmailModel.address',
            cascade='delete, delete-orphan',
            lazy = 'dynamic'
        )
    
    def __init__(self, username, firstname, lastname):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.created = self.lastupdated = datetime.utcnow()

    def json(self, children=False):
        result = {
            'username': self.username,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'created': self.created.isoformat(),
            'lastupdated': self.lastupdated.isoformat()
        }

        if children:
            result['email'] = [email.address for email in self.emails.all()]
        return result

    def save(self, update=False):
        if update:
            self.lastupdated = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).one_or_none()

    @classmethod
    def find_all(cls):
        return cls.query.order_by(cls.username).all()