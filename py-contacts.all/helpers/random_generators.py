import random
import string

def random_str_generator(str_len, allowed_chars):
    return ''.join(random.choice(allowed_chars) for x in range(str_len))

def random_name_generator(name_len):
    return random_str_generator(name_len, string.ascii_lowercase).capitalize()