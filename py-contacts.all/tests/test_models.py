import unittest
from datetime import datetime
from database import database as db
from app import create_app
from models.user import UserModel
from models.email import EmailModel
from models.email_validator import validate_email

T_USERNAME = "uname"
T_FIRSTNAME = "fname"
T_LASTNAME = "lname"
T_FIND_USERNAME = "user"
T_UPDATE_FIRSTNAME = "first"
T_UPDATE_LASTNAME = "last"

T_NAME_PLACEHOLDER = "<string>"
T_USER1 = "uname1"
T_ADDRESS_COM_USER1 = "uname1@example.com"
T_ADDRESS_ORG_USER1 = "uname1@example.org"
T_USER2 = "uname2"
T_ADDRESS_COM_USER2 = "uname2@example.com"
T_ADDRESS_ORG_USER2 = "uname2@example.org"

T_VALID_EMAILS = [
    "user@example.com",
    "first.last@example.co.uk",
    "first_last_22@exam-exam.org",
    "f.last@example.uk",
    "123@example.com"
    ]

T_INVALID_EMAILS = [
    "&user@example.com",
    "us&er@example.com",
    "user#example.com",
    "first.last@example@org",
    "f.last@example.123",
    "user@example",
    "user@example.some",
    ]

class TestEmailValidator(unittest.TestCase):
    def test_valid(self):
        for addr in T_VALID_EMAILS:
            self.assertTrue(validate_email(addr)) 

    def test_invalid(self):
        for addr in T_INVALID_EMAILS:
            self.assertFalse(validate_email(addr))

class TestUserModel(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        with self.app.app_context():
            db.init_app(self.app)
            db.create_all()
            db.session.add(UserModel(T_USER1, T_NAME_PLACEHOLDER, T_NAME_PLACEHOLDER))
            db.session.add(UserModel(T_USER2, T_NAME_PLACEHOLDER, T_NAME_PLACEHOLDER))
            db.session.add(UserModel(T_FIND_USERNAME, T_FIRSTNAME, T_LASTNAME))
            db.session.commit()

    def test_find_all(self):
        with self.app.app_context():
            found = UserModel.find_all()
            self.assertEqual(3, found.__len__())
            self.assertEqual(T_USER1, found[0].username)
            self.assertEqual(T_USER2, found[1].username)
            self.assertEqual(T_FIND_USERNAME, found[2].username)

    def test_find_by_username(self):
        with self.app.app_context():
            found = UserModel.find_by_username(T_FIND_USERNAME)
            self.assertTrue(found)
            self.assertEqual(T_FIND_USERNAME, found.username)
            self.assertEqual(T_FIRSTNAME, found.firstname)
            self.assertEqual(T_LASTNAME, found.lastname)

    def test_insert_model(self):
        with self.app.app_context():
            model = UserModel(T_USERNAME, T_FIRSTNAME, T_LASTNAME)
            model.save()
            inserted = UserModel.find_by_username(T_USERNAME)
            self.assertTrue(inserted)
            self.assertTrue(inserted.created)
            self.assertTrue(inserted.lastupdated)
            self.assertEqual(T_USERNAME, inserted.username)
            self.assertEqual(T_FIRSTNAME, inserted.firstname)
            self.assertEqual(T_LASTNAME, inserted.lastname)

    def test_update_model(self):
        with self.app.app_context():
            model = UserModel.find_by_username(T_FIND_USERNAME)
            model.firstname = T_UPDATE_FIRSTNAME
            model.lastname = T_UPDATE_LASTNAME
            model.save(update=True)
            updated = UserModel.find_by_username(T_FIND_USERNAME)
            self.assertTrue(updated)
            self.assertTrue(updated.lastupdated)
            self.assertLess(updated.created, updated.lastupdated)
            self.assertEqual(T_UPDATE_FIRSTNAME, updated.firstname)
            self.assertEqual(T_UPDATE_LASTNAME, updated.lastname)

    def test_delete_model(self):
        with self.app.app_context():
            model = UserModel.find_by_username(T_FIND_USERNAME)
            model.delete()
            deleted = UserModel.find_by_username(T_FIND_USERNAME)
            self.assertFalse(deleted)

class TestEmailModel(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        with self.app.app_context():
            db.init_app(self.app)
            db.create_all()
            db.session.add(UserModel(T_USER1, T_NAME_PLACEHOLDER, T_NAME_PLACEHOLDER))
            db.session.add(UserModel(T_USER2, T_NAME_PLACEHOLDER, T_NAME_PLACEHOLDER))
            db.session.add(EmailModel(T_ADDRESS_COM_USER1, T_USER1))
            db.session.add(EmailModel(T_ADDRESS_ORG_USER1, T_USER1))
            db.session.add(EmailModel(T_ADDRESS_COM_USER2, T_USER2))
            db.session.commit()

    def test_delete_cascade(self):
        with self.app.app_context():
            user1 = UserModel.find_by_username(T_USER1)
            user1.delete()
            self.assertFalse(EmailModel.find_by_username(T_USER1))
            user2 = UserModel.find_by_username(T_USER2)
            user2.delete()
            self.assertFalse(EmailModel.find_by_username(T_USER2))

    def test_find_all(self):
        with self.app.app_context():
            found = EmailModel.find_all()
            self.assertEqual(3, found.__len__())
            self.assertEqual(T_ADDRESS_COM_USER1, found[0].address)
            self.assertEqual(T_ADDRESS_ORG_USER1, found[1].address)
            self.assertEqual(T_ADDRESS_COM_USER2, found[2].address)

    def test_find_by_username(self):
        with self.app.app_context():
            found = EmailModel.find_by_username(T_USER1)
            self.assertEqual(2, found.__len__())
            self.assertEqual(T_USER1, found[0].username)
            self.assertEqual(T_ADDRESS_COM_USER1, found[0].address)
            self.assertEqual(T_USER1, found[1].username)
            self.assertEqual(T_ADDRESS_ORG_USER1, found[1].address)

    def test_find_by_address(self):
        with self.app.app_context():
            found = EmailModel.find_by_address(T_ADDRESS_COM_USER1)
            self.assertTrue(found)
            self.assertEqual(T_ADDRESS_COM_USER1, found.address)
            self.assertEqual(T_USER1, found.username)  

    def test_insert_model(self):
        with self.app.app_context():
            model = EmailModel(T_ADDRESS_ORG_USER2, T_USER2)
            model.save()
            inserted = EmailModel.find_by_address(T_ADDRESS_ORG_USER2)
            self.assertTrue(inserted)
            self.assertTrue(inserted.created)
            self.assertTrue(inserted.lastupdated)
            self.assertEqual(T_ADDRESS_ORG_USER2, inserted.address)
            self.assertEqual(T_USER2, inserted.username)

    def test_update_model(self):
        with self.app.app_context():
            newuser = UserModel(T_USERNAME, T_FIRSTNAME, T_LASTNAME)
            newuser.save()
            orig_emails = EmailModel.find_by_username(T_USER1)
            self.assertTrue(orig_emails)
            orig_emails_num = len(orig_emails)
            for email in orig_emails:
                email.username = newuser.username
                email.save(update=True)

            new_emails = EmailModel.find_by_username(newuser.username)
            self.assertTrue(new_emails)
            for email in new_emails:
                self.assertTrue(email.lastupdated)
                self.assertLess(email.created, email.lastupdated)
            new_emails_num = len(new_emails)

            old_emails = EmailModel.find_by_username(T_USER1)
            self.assertFalse(old_emails)
            self.assertEqual(orig_emails_num, new_emails_num)

    def test_delete_model(self):
        with self.app.app_context():
            model = EmailModel.find_by_address(T_ADDRESS_ORG_USER1)
            model.delete()
            deleted = EmailModel.find_by_address(T_ADDRESS_ORG_USER1)
            self.assertFalse(deleted)
        
if __name__ == '__main__':
    unittest.main()