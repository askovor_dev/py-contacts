#!/bin/bash
curl "http://localhost:5000/contact"
curl "http://localhost:5000/contact/user1" -X POST -H "Content-Type: application/json" -d '{"firstname":"<string>","lastname":"<string>"}'
curl "http://localhost:5000/contact/user2" -X POST -H "Content-Type: application/json" -d '{"firstname":"<string>","lastname":"<string>"}'
curl "http://localhost:5000/contact/user1/email/user1@example.com" -X POST
curl "http://localhost:5000/contact/user2/email/user2@example.com" -X POST
curl "http://localhost:5000/contact/user1"
curl "http://localhost:5000/contact/user2"
curl "http://localhost:5000/contact/user1/email/user1@example.org" -X POST
curl "http://localhost:5000/contact/user2/email/user2@example.org" -X POST
curl "http://localhost:5000/contact/user1"
curl "http://localhost:5000/contact/user2"
curl "http://localhost:5000/contact/user1" -X PUT -H "Content-Type: application/json" -d '{"firstname":"FirstName1","lastname":"LastName1"}'
curl "http://localhost:5000/contact/user2" -X PUT -H "Content-Type: application/json" -d '{"firstname":"FirstName2","lastname":"LastName2"}'
curl "http://localhost:5000/contact/user1/email/user1@example.com" -X DELETE
curl "http://localhost:5000/contact/user2/email/user2@example.com" -X DELETE
curl "http://localhost:5000/contact/user1"
curl "http://localhost:5000/contact/user2"
curl "http://localhost:5000/contact/user1/email/user1@example.org" -X DELETE
curl "http://localhost:5000/contact/user2/email/user2@example.org" -X DELETE
curl "http://localhost:5000/contact/user1"
curl "http://localhost:5000/contact/user2"
curl "http://localhost:5000/contact/user1" -X DELETE
curl "http://localhost:5000/contact/user2" -X DELETE
curl "http://localhost:5000/contact"
