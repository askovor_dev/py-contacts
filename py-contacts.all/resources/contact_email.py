import logging
import sys
from flask_restful import (Resource, reqparse)
from models.user import UserModel
from models.email import EmailModel
from models.email_validator import validate_email
from resources import MAX_STRING_LEN

_logger = logging.getLogger(__name__)

class ContactEmail(Resource):
    def post(self, username, address):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        if len(address) > MAX_STRING_LEN:
            return {'message': "Email address '{}' is too long.".format(address)}, 400
        if not UserModel.find_by_username(username):
            return {'message': "Contact for user '{}' not found.".format(username)}, 404
        if not validate_email(address):
            return {'message': "Email address '{}' is invalid.".format(address)}, 400
        email = EmailModel.find_by_address(address)
        if email:
            if email.username != username:
                return {'message': "Email address '{}' already registered for user '{}'.".format(address, email.username)}, 400
            else:
                return email.json(), 200

        email = EmailModel(address, username)
        try:
            email.save()
        except:
            _logger.error("Internal error: %s", sys.exc_info())
            return None, 500

        return email.json(), 201

    def delete(self, username, address):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        if len(address) > MAX_STRING_LEN:
            return {'message': "Email address '{}' is too long.".format(address)}, 400
        if not validate_email(address):
            return {'message': "Email address '{}' is invalid.".format(address)}, 400
        email = EmailModel.find_by_address(address)
        if not email:
            return {'message': "Email address '{}' not found.".format(address)}, 404
        if email.username != username:
            return {'message': "Email address '{}' is not owned by user '{}'.".format(address, username)}, 400

        try:
            email.delete()
        except:
            _logger.error("Internal error: %s", sys.exc_info())
            return None, 500

        return None, 200