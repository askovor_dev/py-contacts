import logging
import sys
from flask_restful import (Resource, reqparse)
from models.user import UserModel
from resources import MAX_STRING_LEN

_logger = logging.getLogger(__name__)

class Contact(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('firstname',
                        type=str,
                        required=True,
                        help="This field cannot be blank."
                        )
    parser.add_argument('lastname',
                        type=str,
                        required=True,
                        help="This field cannot be blank."
                        )

    def get(self, username):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        user = UserModel.find_by_username(username)
        if user:
            return user.json(children=True), 200
        return {'message': 'Not found'}, 404

    def post(self, username):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        if UserModel.find_by_username(username):
            return {'message': "Contact for user '{}' already exists.".format(username)}, 400

        data = Contact.parser.parse_args()
        if len(data['firstname']) > MAX_STRING_LEN:
            return {'message': "Firstname '{}' is too long.".format(data['firstname'])}, 400
        if len(data['lastname']) > MAX_STRING_LEN:
            return {'message': "Lastname '{}' is too long.".format(data['lastname'])}, 400
        user = UserModel(username, **data)
        try:
            user.save()
        except:
            _logger.error("Internal error: %s", sys.exc_info())
            return None, 500

        return user.json(children=False), 201

    def put(self, username):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        user = UserModel.find_by_username(username)
        if not user:
            return {'message': "Contact for user '{}' not found.".format(username)}, 404

        data = Contact.parser.parse_args()
        if len(data['firstname']) > MAX_STRING_LEN:
            return {'message': "Firstname '{}' is too long.".format(data['firstname'])}, 400
        if len(data['lastname']) > MAX_STRING_LEN:
            return {'message': "Lastname '{}' is too long.".format(data['lastname'])}, 400
        
        user.firstname = data ['firstname']
        user.lastname = data ['lastname']
        try:
            user.save(update=True)
        except:
            _logger.error("Internal error: %s", sys.exc_info())
            return None, 500

        return user.json(children=True), 200

    def delete(self, username):
        if len(username) > MAX_STRING_LEN:
            return {'message': "Username '{}' is too long.".format(username)}, 400
        user = UserModel.find_by_username(username)
        if not user:
            return {'message': 'Not found'}, 404
            
        try:
            user.delete()
        except:
            _logger.error("Internal error: %s", sys.exc_info())
            return None, 500

        return None, 200

class ContactList(Resource):
    def get(self):
        return {'contacts': [u.json(children=True) for u in UserModel.find_all()]}, 200