Current solution is in folder "py-contacts.all"

Setup & Run "py-contacts.all" solution:
1. Setup virtual env using 'pipenv' (with Pipfile) or 'virtualenv' (with requirements.txt)
2. Run API in virtual env by 'python app.py' command.
   On Linux you can also use provided 'run_app.sh' bash script.
3. Open site 'http://localhost:5000/' in browser.
   You can test API using provided Swagger UI.
4. Run tasks in virtual env by 'celery -A tasks.api_tasks worker -B --loglevel=info' command.
   On Linux you can also use provided 'run_tasks.sh' bash script.
5. Unit tests currently done for models only ('tests.test_models').